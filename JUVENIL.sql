/* Enunciado de la práctica juvenil de PL/SQL
 * Autor: Carlos Masana
 * Descripción: PL/SQL
 * Fecha: 28/04/19
 */

/* Ejercicio 1: Crear tablas */
DROP TABLE socios CASCADE CONSTRAINTS;

CREATE TABLE socios (
dni VARCHAR2(10) NOT NULL,
nombre VARCHAR2(20) NOT NULL,
direccion VARCHAR2(20),
penalizaciones NUMBER(2) DEFAULT 0,
CONSTRAINT socios_pk PRIMARY KEY (dni)
);

DROP TABLE libros CASCADE CONSTRAINTS;

CREATE TABLE libros (
reflibro VARCHAR2(10) NOT NULL,
nombre VARCHAR2(30) NOT NULL,
autor VARCHAR2(20) NOT NULL,
genero VARCHAR2(10),
anopublicacion NUMBER,
editorial VARCHAR2(10),
CONSTRAINT libros_pk PRIMARY KEY (reflibro)
);

DROP TABLE prestamos CASCADE CONSTRAINTS;

CREATE TABLE prestamos (
dni VARCHAR2(10) NOT NULL,
reflibro VARCHAR2(10) NOT NULL,
fechaprestamo DATE NOT NULL,
duracion NUMBER(2) DEFAULT 24,
CONSTRAINT prestamos_pk PRIMARY KEY (dni, reflibro, fechaprestamo),
CONSTRAINT dni_fk FOREIGN KEY (dni)
REFERENCES socios (dni)
ON DELETE CASCADE,
CONSTRAINT reflibro_fk FOREIGN KEY (reflibro)
REFERENCES libros (reflibro)
ON DELETE CASCADE
);

/* Ejercicio 1: Insertar datos */
INSERT INTO socios (dni, nombre, direccion)
VALUES ('32009627L','Manolo','Castadón');

INSERT INTO socios
VALUES ('45009648Z','Jose','Barcelona',0);

INSERT INTO socios
VALUES ('44809667C','Concepción','Ourense',2);

INSERT INTO socios
VALUES ('31809689K','Teresa','Navea',0);

----

INSERT INTO libros
VALUES ('LIBRO01','A Esmorga','E. Blanco Amor','Novela',1959,'Paraugas');

INSERT INTO libros
VALUES ('LIBRO02','Estacións ao mar','Xohana Torres','Poesía',1980,'Bolboreta');

INSERT INTO libros
VALUES ('LIBRO03','Poesía última de amor','Lois Pereiro','Poesía',1995,'Laranxa');

INSERT INTO libros
VALUES ('LIBRO04','O crepúsculo e as formigas','XL. Méndez Ferrín','Novela',1961,'Langrán');

INSERT INTO libros
VALUES ('LIBRO05','Á marxe. Palabra de escritor','Carlos Casares','Ensayo',2003,'Luscofusco');

INSERT INTO libros
VALUES ('LIBRO06','Galván en Saor','Darío Xohán Cabana','Novela',1989,'Lambón');

----

INSERT INTO prestamos
VALUES ('32009627L','LIBRO01',TO_DATE('10-02-2019','dd-MM-yyyy'),24);

INSERT INTO prestamos
VALUES ('32009627L','LIBRO02',TO_DATE('02-04-2019','dd-MM-yyyy'),24);

INSERT INTO prestamos
VALUES ('45009648Z','LIBRO01',TO_DATE('05-08-2018','dd-MM-yyyy'),24);

INSERT INTO prestamos
VALUES ('44809667C','LIBRO03',TO_DATE('02-04-2019','dd-MM-yyyy'),24);

INSERT INTO prestamos
VALUES ('31809689K','LIBRO04',TO_DATE('21-11-2018','dd-MM-yyyy'),12);

INSERT INTO prestamos
VALUES ('31809689K','LIBRO06',TO_DATE('18-01-2019','dd-MM-yyyy'),24);

----

SET SERVEROUTPUT ON;

/* Ejercicio 1: Realiza un procedimiento llamado listadocuatromasprestados que nos muestre
por pantalla un listado de los cuatro libros más prestados y los socios a los que han sido prestados */
CREATE OR REPLACE PROCEDURE listadocuatromasprestados
IS      
    v_lib_pres libros%ROWTYPE;
    
    CURSOR c_librosprestados IS
        SELECT reflibro, COUNT(reflibro) AS veces
        FROM prestamos
        GROUP BY reflibro
        ORDER BY veces DESC
        FETCH FIRST 4 ROWS ONLY;

BEGIN
    FOR r_librosprestados IN c_librosprestados LOOP
        SELECT * INTO v_lib_pres FROM libros
        WHERE reflibro = r_librosprestados.reflibro;
        
        dbms_output.put_line(v_lib_pres.nombre||' '||r_librosprestados.veces||' '||v_lib_pres.genero);
        mostrarusuarios(r_librosprestados.reflibro);
    END LOOP;

END listadocuatromasprestados;

/* Comprobación */
BEGIN
    listadocuatromasprestados;
END;


CREATE OR REPLACE PROCEDURE mostrarusuarios (v_reflibro VARCHAR2) 
IS  
    CURSOR c_libros_socios IS
        SELECT dni, fechaprestamo
        FROM prestamos
        WHERE reflibro = v_reflibro;
BEGIN
    FOR r_libros_socios IN c_libros_socios LOOP
        DBMS_OUTPUT.PUT_LINE(r_libros_socios.dni||' '||r_libros_socios.fechaprestamo);
    END LOOP;
END mostrarusuarios;
