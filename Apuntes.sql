/* Apuntes PL/SQL
 * Autor: Carlos Masana
 * Descripción: PL/SQL
 * Fecha: 28/04/19
 */
 
 /* Tipos de datos */
 -- CHAR (Cadenas de carácteres de longitud fija)
 -- VARCHAR2 (Cadenas de carácteres de longitud variable)
 -- LONG (Cadenas de carácteres de longitud variable MUY LARGAS)
 -- NUMBER (Números)
 -- BOOLEAN (TRUE y FALSE)
 -- DATE (Fecha y hora) -- Por defecto 'dd-mm-aaaa'
 
 /* Declaración de variables */
 -- v_salario INT DEFAULT 0;                -- Asignando un valor por defecto
 -- v_salario2 emp.sal%TYPE;                -- Tipo columna (Podemos usarlo también a partir de una variable)
 -- v_palabra VARCHAR2(10);                 
 -- v_empleado emp%ROWTYPE;                 -- Tipo tabla
 -- v_dni CHAR(9) NOT NULL := 'ABCDEFGH9';  -- Forzar variable para que siempre contenga un valor
 
 /* Declaración de constantes */
 -- v_salario CONSTANT NUMBER := 1000;      -- Utilizamos la palabra CONSTANT después del nombre y asignamos un valor
 
 /* Imprimir por pantalla */
 -- SET SERVEROUTPUT ON;
 -- DBMS_OUTPUT.PUT_LINE('Texto'||v_salario||'Texto2');
 
 /* Asignar consulta a una variable */
 DECLARE
    empleado emp%ROWTYPE;
 BEGIN
    SELECT * INTO empleado -- Utilizamos INTO para asignar consulta a variable
    FROM emp
    WHERE empno = 7900;
    
    DBMS_OUTPUT.PUT_LINE('El nombre es'||empleado.ename);
 END;
 
 /* Estructura condicionales */
 IF (condicion) THEN
    -- código;
 ELSIF (condicion) THEN
    -- código;
 ELSE
    -- código;
 END IF;
 
 /* Estrutura bucles */
 
 -- Bucle simple
 LOOP
    -- código;
 END LOOP;
 
 -- Bucle condicional (WHILE)
 WHILE condicion LOOP
    -- código;
 END LOOP;
 
 -- Bucle numérico (FOR)
 FOR v_contador IN 0..50 LOOP
    -- código;
 END LOOP;
 
 -- Bucles sobre sentencias SELECT
 FOR registro IN (SELECT empno, ename FROM emp) LOOP
    DBMS_OUTPUT.PUT_LINE(registro.empno||registro.ename);
 END LOOP;
 
 /* Crear registros */
 DECLARE
   TYPE info_empleado IS RECORD
   (
       nombre emp.ename%TYPE;
       salario INT;
   )
   v_empleado info_empleado;
 BEGIN
    SELECT ename INTO v_empleado.nom FROM emp WHERE empno = 7900;
    v_empleado.salario := 1000;
    DBMS_OUTPUT.PUT_LINE(v_empleado.nom || v_empleado.salario);
 END;
 
 /* Crear varray (Array que contiene elementos del mismo tipo) */
 DECLARE
    TYPE tres_palabras IS VARRAY(3) OF VARCHAR2(10);
    mi_nombre tres_palabras := tres_palabras('Carlos','Jessica','Meiga');
 BEGIN
    DBMS_OUTPUT.PUT_LINE(mi_nombre(1));
    DBMS_OUTPUT.PUT_LINE(mi_nombre(2));
    DBMS_OUTPUT.PUT_LINE(mi_nombre(3));
 END;
 
 /* Sintaxis cursor */
 
 -- Creación de cursor
 CURSOR "nombre_cursor" IS "sentencia SELECT";
 
 -- Declaración del registro donde guardar cada elemento del cursor
 "nombre_registro" "nombre_cursor"%ROWTYPE;
 
 -- Apertura del cursor
 OPEN nombre_cursor;
 
 -- Cargar el registro actual
 FETCH nombre_cursor INTO (variable, array, registro);
 -- FETCH c_dept INTO r_dept;
 
 -- Salida del cursor
 EXIT WHEN nombre_cursor%NOTFOUND;
 -- EXIT WHEN c_dept%NOTFOUND;
 
 -- Cierre del cursor
 CLOSE nombre_cursor;
 
 -- BUCLE ESPECIAL PARA CURSORES
 FOR nombre_registro IN nombre_cursor LOOP
    -- código;
 END LOOP;
 
 
 /* Excepciones más frecuentes */
 -- NO_DATA_FOUND  --- SELECT INTO no retorna ningún valor
 -- TOO_MANY_ROWS  --- SELECT INTO retorna más de una fila
 -- OTHERS THEN RAISE_APPLICATION_ERROR   --- Cualquier otro tipo
 
 /* Crear excepciones personalizadas */
 -- 1. Declaramos excepción en la zona de variables
 -- mi_excepción EXCEPTION;
 
 -- 2. Condición que hará saltar nuestra excepción
 -- IF ename = 'Masana' THEN
 --     RAISE mi_excepcion;
 
 -- 3. Crear zona de excepciones entre begin y end
 -- EXCEPTION
 --     WHEN mi_excepcion THEN
 --         DBMS_OUTPUT.PUT_LINE('El empleado Masana es intocable');
 --     WHEN no_data_found THEN
 --         DBMS_OUTPUT.PUT_LINE('No existe el usuario con este número');
 -- END;
 
 /* Creación de triggers */
 CREATE OR REPLACE TRIGGER nombre
   [BEFORE | AFTER]
   [DELETE | INSERT | UPDATE OF columnas] OR [DELETE | INSERT | UPDATE OF ON tabla]
   [FOR EACH ROW [WHEN condicion disparo]]
[DECLARE]
  -- Declaración de variables locales
BEGIN
  -- Instrucciones de ejecución
[EXCEPTION]
  -- Instrucciones de excepción
END;
 
 
 

 
 