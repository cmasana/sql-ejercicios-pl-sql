SET SERVEROUTPUT ON;

/* 1. Procedimiento que muestra el nombre y el salario del empleado cuyo código es 2 */
CREATE OR REPLACE PROCEDURE nombre_emp(
n_emp VARCHAR)
IS
    v_emp employees%ROWTYPE;
BEGIN
    SELECT * INTO v_emp FROM employees WHERE employee_id = 2;
    DBMS_OUTPUT.PUT_LINE('El nombre es ' || v_emp.first_name || ' y su salario es de ' || v_emp.salary || ' euros.');
END;
/

/* 2. Procedimiento que recibe como parámetro un código de empleado y devuelve su nombre */
CREATE OR REPLACE PROCEDURE nombre_emp(
c_employee_id NUMBER)
IS
    nombre_emp employees%ROWTYPE;
BEGIN
    SELECT * INTO nombre_emp
    FROM employees
    WHERE employee_id = c_employee_id;
    DBMS_OUTPUT.PUT_LINE('El nombre del empleado es: '|| nombre_emp.first_name || ' ' || nombre_emp.last_name);
END;
/

/* Comprobación */
CALL nombre_emp (2);

/* 3. Crear un procedimiento PL/SQL que cuente el número de filas que hay en la tabla EMP (de Scott),
deposita el resultado en una variable y visualiza su contenido. */
-- NOTA: Error al crear el procedimiento en la tabla EMP de Scott, desde cualquiera de las tablas de CMASANA funciona correctamente
CREATE OR REPLACE PROCEDURE  contarfilas(
v_contar OUT NUMBER)
IS
BEGIN
    SELECT COUNT(*) INTO v_contar FROM CLIENTS;
    DBMS_OUTPUT.PUT_LINE('El número de filas es: '|| v_contar); 
END;
/

/* 4. Codificar un procedimiento que reciba una cadena y la visualice al revés. */
CREATE OR REPLACE PROCEDURE cadena_reves(
v_reves VARCHAR)
IS
    v_cadena VARCHAR(20);
BEGIN
    v_cadena := v_reves;
    SELECT REVERSE(v_cadena) AS REVES INTO v_cadena FROM DUAL;
    DBMS_OUTPUT.PUT_LINE(v_cadena);
END;
/

/* COMPROBACIÓN */
CALL cadena_reves('Hola');

/* 5. Escribir un procedimiento que reciba una fecha y escriba el año, en número, correspondiente a esa fecha. */
CREATE OR REPLACE PROCEDURE year_date(
i_fecha VARCHAR)
IS
    v_fecha DATE;
    v_number NUMBER;
BEGIN
    v_fecha := TO_DATE(i_fecha,'DD-MM-RR');
    
    SELECT EXTRACT(YEAR FROM v_fecha) YEAR INTO v_number FROM DUAL;
    DBMS_OUTPUT.PUT_LINE(v_number);
END;
/

CALL year_date('10-01-19');

/* 6. Codificar un procedimiento que reciba una lista de hasta 5 números y visualice su suma */
CREATE OR REPLACE PROCEDURE suma_num(
i_number NUMBER, i2_number NUMBER DEFAULT 0, i3_number NUMBER DEFAULT 0, i4_number NUMBER DEFAULT 0, i5_number NUMBER DEFAULT 0)
IS
    v_number NUMBER;
BEGIN
    SELECT SUM(i_number + i2_number  + i3_number + i4_number + i5_number) TOTAL INTO v_number FROM DUAL;
    DBMS_OUTPUT.PUT_LINE(v_number);
END;
/

/* Comprobación */
CALL suma_num(1,3,4);

/* 7. Implementar un procedimiento que reciba un importe y visualice el desglose del cambio en unidades monetarias
de 0.01, 0.02, 0.05, 0.10, 0.20, 0.50, 1, 2, 5, 10, 20, 50, 100, 200, 500 y 1000€ en orden inverso al que aparecen aquí enumeradas. */
CREATE OR REPLACE PROCEDURE monedas(
imp_tot NUMBER)
IS
    camb_eur NUMBER;
    moneda NUMBER;
    unidad NUMBER;
BEGIN
    camb_eur := imp_tot;
    DBMS_OUTPUT.PUT_LINE('Desglose de '|| imp_tot);
    WHILE camb_eur > 0
    LOOP
        IF camb_eur >= 1000 THEN moneda := 1000;
            ELSIF camb_eur >= 500 THEN moneda := 500;
            ELSIF camb_eur >= 200 THEN moneda := 200;
            ELSIF camb_eur >= 100 THEN moneda := 100;
            ELSIF camb_eur >= 50 THEN moneda := 50;
            ELSIF camb_eur >= 20 THEN moneda := 20;
            ELSIF camb_eur >= 10 THEN moneda := 10;
            ELSIF camb_eur >= 5 THEN moneda := 5;
            ELSIF camb_eur >= 2 THEN moneda := 2;
            ELSIF camb_eur >= 1 THEN moneda := 1;
            ELSIF camb_eur >= 0.50 THEN moneda := 0.50;
            ELSIF camb_eur >= 0.20 THEN moneda := 0.20;
            ELSIF camb_eur >= 0.10 THEN moneda := 0.10;
            ELSIF camb_eur >= 0.05 THEN moneda := 0.05;
            ELSIF camb_eur >= 0.02 THEN moneda := 0.02;
            ELSE moneda := 0.01;
        END IF;
        unidad := TRUNC(camb_eur/moneda);
        DBMS_OUTPUT.PUT_LINE(unidad||' Unidades de: '||moneda ||' €');
        camb_eur := MOD(camb_eur,moneda);
    END LOOP;
END;
/

CALL monedas(5567.7);

/* 8. Codificar un procedimiento que permita borrar a un empleado cuyo número se pasará en llamada */
CREATE OR REPLACE PROCEDURE del_emp(
id_emp NUMBER)
IS
    v_emp employees.employee_id%TYPE;
BEGIN
    v_emp := id_emp;
    DELETE FROM employees WHERE employee_id = v_emp;
END;
/

CALL del_emp(36);

/* 9. Escribir un procedimiento que modifique la localidad de un departamento.
El procedimiento recibirá como parámetros el número de departamento y la localidad nueva */
CREATE OR REPLACE PROCEDURE mod_loc_dpt(
id_dpto NUMBER, loc_dpto VARCHAR)
IS
BEGIN
    UPDATE
        (
        SELECT l.city, d.department_id
        FROM locations l
        INNER JOIN departments d
        ON l.location_id = d.location_id
        AND d.department_id = id_dpto
        )
    SET city = loc_dpto;
END;
/

/* Comprobación */
SELECT l.city, d.department_id
FROM locations l
INNER JOIN departments d
ON l.location_id = d.location_id
AND d.department_id = 2;

CALL mod_loc_dpt(2,'Ourense');

/* 10. Visualizar todos los procedimientos y funciones de usuarios almacenados en la bbdd y su situación (valid o invalid) */
SELECT owner, object_name, object_type, status FROM all_objects --NOTA: Objetos accesibles para el usuario actual (utilizar dba_objects para todos)
WHERE object_type = 'PROCEDURE'
OR object_type = 'FUNCTION';

/* 11. Realizar un procedimiento que reciba un número y muestre su tabla de multiplicar */
CREATE OR REPLACE PROCEDURE multiplica(
i_num NUMBER)
IS
    v_num NUMBER;
    v_contador NUMBER;
BEGIN
    v_num := i_num;
    v_contador := 1;
    DBMS_OUTPUT.PUT_LINE('La tabla de multiplicar del número '||v_num || ' es: ');
    WHILE v_contador < 11
        LOOP
           DBMS_OUTPUT.PUT_LINE(v_num ||' x '||v_contador||' = '|| v_num * v_contador);
           v_contador := v_contador + 1;
        END LOOP;
END;
/

/* Comprobación */
CALL multiplica(9);

    



